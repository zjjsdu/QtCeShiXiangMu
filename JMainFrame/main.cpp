﻿#include "jmainframe.h"
#include <QApplication>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    JmainFrame w;
    w.show();
    QFile file(":/file/qss/style.qss");
    file.open(QIODevice::ReadOnly);
    QString qss = QLatin1String(file.readAll());
    qApp->setStyleSheet(qss);
    file.close();
    return a.exec();
}
