﻿#ifndef JMAINFRAME_H
#define JMAINFRAME_H

#include <QWidget>
#include <QList>
#include <QMouseEvent>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLabel>

//调用WIN API需要用到的头文件与库 [实现缩放]
#ifdef Q_OS_WIN
#include <qt_windows.h>
#include <windowsx.h>
#endif

class JButton : public QWidget
{
    Q_OBJECT
public:
    enum IconOrientation {UP, DOWN, LEFT, RIGHT};
    struct JRatio{int iconRatio; int textRatio;};
    explicit JButton(int Orientation = UP, QWidget *parent = nullptr);
    explicit JButton(const QIcon &icon, const QString &text, int Orientation = UP, QWidget *parent = nullptr);
    explicit JButton(const QPixmap &icon, const QString &text, int Orientation = UP, QWidget *parent = nullptr);
    JButton(const JButton &btn);
    virtual ~JButton() override;
    QSize size() const;
    void setSize(const QSize &size);
    void setSize(int width, int height);
    JRatio ratio() const;
    void setRatio(const JRatio &ratio);
    void setRatio(int iconRatio, int textRatio);
    void setOrientation(int Orientation);
    void setIcon(const QIcon &icon);
    void setIcon(const QPixmap &icon);
    void setText(const QString &text);
signals:
    void clicked();
protected:
    virtual void mouseReleaseEvent(QMouseEvent *event) override;
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *event) override;
    virtual void mouseDoubleClickEvent(QMouseEvent *event) override;
private:
    int btnOrientation;
    QSize btnSize;
    JRatio btnRatio;
    QLabel *labelIcon;
    QLabel *labelText;
    QVBoxLayout *vLayMain;
    QPixmap btnIcon;
    QString btnText;
private:
    void Init(int Orientation);
    void setBtnSize(const QSize &size);
    void setBtnRatio(const JRatio &ratio);
};






class JTitleBar : public QWidget
{
    Q_OBJECT
public:
    explicit JTitleBar(QWidget *parent = nullptr);
    JTitleBar(const JTitleBar &title);
    virtual ~JTitleBar() override;
    // 设置标题栏高度
    virtual void setTitleHeight(int height = 100);
    // 设置Logo宽度
    virtual void setLogoWidth(int width = 150);
    // 添加一个带图标的Button
    virtual void addJButton(JButton *button);
    // 设置图标间距
    void setJButtonSpacing(int size);
    // 设置LOGO
    void setLogo(const QIcon &icon);
    void setLogoStyle(const QString &style);
    // 设置最大化、最小化、关闭按钮样式
    void setWindowMinStyle(const QString &style);
    void setWindowMaxStyle(const QString &style);
    void setWindowCloseStyle(const QString &style);
private:
    void Init();
signals:
    void showMin();
    void showMax();
    void close();
    void doubleClieck();
    void mousePress(const QMouseEvent &event);
    void mouseRelease();
    void mouseMove(const QMouseEvent *event);
protected slots:
    void btnMin_clicked();
    void btnMax_clicked();
    void btnClose_clicked();
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;
protected:
    // 控件
    QPushButton *btnLogo;
    QHBoxLayout *hLayTitle;
    QHBoxLayout *hLayButtons;
    QList<JButton*> btnList;
    QPushButton *btnMin;
    QPushButton *btnMax;
    QPushButton *btnClose;
    QHBoxLayout *hLayWindow;
    QVBoxLayout *vLayWindow;
    // 属性
    int titleHeight;
    int logoWidth;
    QPoint startPos;
    bool leftButtonState;
};




class JmainFrame : public QWidget
{
    Q_OBJECT
public:
    explicit JmainFrame(QWidget *parent = nullptr);
    ~JmainFrame();
public:
    QWidget *widgetMain;
    enum WINSTATUS {NORMAL, MAXIMIZED, MINIMIZED};
protected slots:
    void showMin();
    void showMax();
    void colseWin();
    void titleDoubleCilck();
    void mousePress(const QMouseEvent &sPos);
    void mouseRelease();
    void mouseMove(const QMouseEvent *mPos);
protected:
    virtual bool nativeEvent(const QByteArray &eventType, void *message, long *result) override;
private:
    // 控件
    JTitleBar *widgetTitle;
    QVBoxLayout *vLayMain;
    int WinStatus;
    QPoint startPos;
    bool dragMax;
};
#endif // JMAINFRAME_H
