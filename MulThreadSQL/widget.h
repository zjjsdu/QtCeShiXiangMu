﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QThread>
#include <QTimer>
#include "thread1.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_pushButton_clicked();
    void timertimeout();
private:
    Ui::Widget *ui;
    Thread1 *thd1;
    Thread1 *thd2;
    QThread *td1;
    QThread *td2;
    QTimer *timer;
    bool IsRun;
signals:
    void StartWork();
};

#endif // WIDGET_H
