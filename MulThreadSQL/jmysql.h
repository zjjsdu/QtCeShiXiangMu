﻿#ifndef JMYSQL_H
#define JMYSQL_H

#include <QObject>
#include <QtSql>
#include <QVector>
#include <QDateTime>

struct DataBaseInfo
{
    QString Server;
    QString User;
    QString Passwd;
    QString DbName;
    DataBaseInfo() {
        Server = "";
        User = "";
        Passwd = "";
        DbName = "";
    }
};

struct AreaInfo
{
    int AreaId;
    int Type;
    QString AreaName;
    AreaInfo() {
        AreaId = -1;
        Type = -1;
        AreaName = "";
    }
};

class JMySQL : public QObject
{
    Q_OBJECT
public:
    explicit JMySQL(QObject *parent = nullptr);
    JMySQL(DataBaseInfo db);
    JMySQL(DataBaseInfo db, QString ConnectName);
    ~JMySQL();
private:
    DataBaseInfo DbInfo;
    QSqlDatabase db;
    void Init(QString ConnectName = "");
public:
    // 读取区域列表
    bool ReadAreaInfo(QVector<AreaInfo> &List);
};

#endif // JMYSQL_H
