#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "logger.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setLogMode(LM_FILE);
//    DatabaseInfo info("127.0.0.1", "root", "", "log");
//    setLogDatabaseInfo(info);
//    setLogNetPort(8989);
    setLogFilePath(QApplication::applicationDirPath());
    qInstallMessageHandler(log);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    QString str = ui->lineEdit->text();
    qInfo() << str;
    ui->textEdit->append(str);
}
