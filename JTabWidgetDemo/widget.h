﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

protected slots:
    void btnSwitch_clicked();
    void btnSwitchDir_clicked();
    void btnSwitchStyle_clicked();
private:
    void InitStyle();
private:
    Ui::Widget *ui;
    // 变量
    QStringList styleList;
    // 控件
    QPushButton *btnSwitchTab;
    QPushButton *btnSwitchDir;
    QPushButton *btnSwitchStyle;
    QHBoxLayout *hLayOP;
    QWidget *Tab1;
    QWidget *Tab2;
    QWidget *Tab3;
    QTabWidget *tabWidgetMain;
    QVBoxLayout *vLayMain;
};
#endif // WIDGET_H
