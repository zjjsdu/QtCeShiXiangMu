﻿#include "widget.h"
#include "ui_widget.h"
#include "chlistdlg.h"
#include <QSettings>

#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8")
#endif


Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    m_filterType(LOWPASS),
    m_winType(RECTANGLE),
    m_winLength(64),
    m_freq(1000),
    m_freqLowLimit(100),
    m_freqUpLimit(300),
    m_filter(m_filterType, m_winType, m_winLength, m_freq, m_freqLowLimit, m_freqUpLimit)
{
    ui->setupUi(this);

    QSettings s("config.ini", QSettings::IniFormat);
    m_fileName = s.value("/General/Path").toString();

    int temp = s.value("/General/FileType").toInt();
    ui->comboBox->setCurrentIndex(temp);

    temp = s.value("/Filter/FilterType").toInt();
    m_filterType = FilterType(temp);
    ui->comboBox_2->setCurrentIndex(temp);

    temp = s.value("/Filter/WinType").toInt();
    m_winType = FilterWindowType(temp);
    ui->comboBox_3->setCurrentIndex(temp);

    m_winLength = s.value("/Filter/WinLength").toInt();
    ui->spinBox->setValue(m_winLength);

    m_freq = s.value("/Filter/Freq").toInt();
    ui->spinBox_2->setValue(m_freq);

    m_freqUpLimit = s.value("/Filter/UpFreq").toInt();
    ui->spinBox_3->setValue(m_freqUpLimit);

    m_freqLowLimit = s.value("/Filter/LowFreq").toInt();
    ui->spinBox_4->setValue(m_freqLowLimit);

    m_filter.setFilterType(m_filterType);
    m_filter.setFilterWinType(m_winType);
    m_filter.setWinLength(m_winLength);
    m_filter.setFreqency(m_freq);
    m_filter.setFreqLower(m_freqLowLimit);
    m_filter.setFreqUpper(m_freqUpLimit);

    initChartWave();

    showMaximized();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::initChartWave()
{
    ChartWave::ChInfo info(1, "graph", 40000);

    ui->wd1->setFrequencyWaveVisible(false);
    ui->wd1->addGraph(info);
    ui->wd1->setTimeTitle("原始波形");
    ui->wf1->setFrequencyWaveVisible(false);
    ui->wf1->addGraph(info);
    ui->wf1->setTimeTitle("原始波形频谱");
    ui->wd2->setFrequencyWaveVisible(false);
    ui->wd2->addGraph(info);
    ui->wd2->setTimeTitle("冲激响应");
    ui->wf2->setFrequencyWaveVisible(false);
    ui->wf2->addGraph(info);
    ui->wf2->setTimeTitle("冲激响应频谱");
    ui->wd3->setFrequencyWaveVisible(false);
    ui->wd3->addGraph(info);
    ui->wd3->setTimeTitle("滤波输出波形");
    ui->wf3->setFrequencyWaveVisible(false);
    ui->wf3->addGraph(info);
    ui->wf3->setTimeTitle("输出波形频谱");
}

QVector<double> Widget::readDataFile()
{
    m_fileName = QFileDialog::getOpenFileName(this, "选择数据文件", m_fileName);
    if(m_fileName.isEmpty())
        return QVector<double>();

    QSettings s("config.ini", QSettings::IniFormat);
    s.setValue("/General/Path", m_fileName);


    QVector<double> value;
    DataFileType ft = DataFileType(ui->comboBox->currentIndex());
    QFile fd(m_fileName);
    if(FT_TXT == ft)
    {
        if(!fd.open(QIODevice::ReadOnly | QIODevice::Text))
            return QVector<double>();
        QString str = fd.readAll();
        if(str.isEmpty())
        {
            QMessageBox::information(this, "提示", "数据文件中无数据");
            return QVector<double>();
        }

        str = str.replace(' ', ',');
        str = str.replace(' ', ',');
        str = str.replace('\n', ',');
        QStringList data = str.split(",");
        for(auto d : data)
        {
            value.append(d.toDouble());
        }
    }
    else if(FT_WDAT == ft)
    {
        // 该部分代码已删除
    }
    fd.close();

    return value;
}

// 原始数据
void Widget::on_pushButton_clicked()
{
    m_keys.clear();
    m_values.clear();
    m_values = readDataFile();
    if(m_values.isEmpty())
        return;

    for(int i = 0; i < m_values.size(); i++)
        m_keys.append(i);

    double xlower = 0, xupper = 0, ylower = 0, yupper = 0;

    xlower = m_keys.front();
    xupper = m_keys.back();
    ylower = *std::min_element(std::begin(m_values), std::end(m_values));
    yupper = *std::max_element(std::begin(m_values), std::end(m_values));
    // 原始波形--显示波形
    ui->wd1->setTimeGraphData(1, m_keys, m_values);
    ui->wd1->setTimeXRange(xlower, xupper);
    ui->wd1->setTimeYRange(ylower, yupper);

    // 冲击响应
    on_pushButton_4_clicked();

    // 滤波
    on_pushButton_2_clicked();

    // 频谱
    on_pushButton_3_clicked();
}

// 冲击信号
void Widget::on_pushButton_4_clicked()
{
    m_impKeys.clear();
    m_impValues.clear();
    m_impValues = m_filter.ImpactData;
    if(m_impValues.isEmpty())
        return;

    for(int i = 0; i < m_impValues.size(); i++)
        m_impKeys.append(i);

    double xlower = 0, xupper = 0, ylower = 0, yupper = 0;

    xlower = m_impKeys.front();
    xupper = m_impKeys.back();
    ylower = *std::min_element(std::begin(m_impValues), std::end(m_impValues));
    yupper = *std::max_element(std::begin(m_impValues), std::end(m_impValues));
    // 冲击响应--显示波形
    ui->wd2->setTimeGraphData(1, m_impKeys, m_impValues);
    ui->wd2->setTimeXRange(xlower, xupper);
    ui->wd2->setTimeYRange(ylower, yupper);
}

// 滤波
void Widget::on_pushButton_2_clicked()
{
    m_filter.Convolution(m_values);
    m_fValues = m_filter.OutData;

    double xlower = 0, xupper = 0, ylower = 0, yupper = 0;

    xlower = m_keys.front();
    xupper = m_keys.back();
    ylower = *std::min_element(std::begin(m_fValues), std::end(m_fValues));
    yupper = *std::max_element(std::begin(m_fValues), std::end(m_fValues));
    // 原始波形--显示波形
    ui->wd3->setTimeGraphData(1, m_keys, m_fValues);
    ui->wd3->setTimeXRange(xlower, xupper);
    ui->wd3->setTimeYRange(ylower, yupper);
}

// 频谱
void Widget::on_pushButton_3_clicked()
{
    // 原始波形频谱
    QVector<qreal> keys, fvalues;
    frequencySpectrum(m_values, fvalues);
    int flen = (fvalues.size() - 1) * 2;
    for(int i = 0; i < fvalues.size(); i++)
        keys.append(m_freq / flen * i);

    double xlower = 0, xupper = 0, ylower = 0, yupper = 0;

    xlower = keys.front();
    xupper = keys.back();
    ylower = *std::min_element(std::begin(fvalues), std::end(fvalues));
    yupper = *std::max_element(std::begin(fvalues), std::end(fvalues));
    // 原始波形频谱--显示波形
    ui->wf1->setTimeGraphData(1, keys, fvalues);
    ui->wf1->setTimeXRange(xlower, xupper);
    ui->wf1->setTimeYRange(ylower, yupper);


    // 冲击响应信号频谱
    QVector<qreal> ivalues;
    frequencySpectrum(m_impValues, ivalues);
    keys.clear();
    flen = ivalues.size();
    for(int i = 0; i < ivalues.size(); i++)
        keys.append(i);

    xlower = keys.front();
    xupper = keys.back();
    ylower = *std::min_element(std::begin(ivalues), std::end(ivalues));
    yupper = *std::max_element(std::begin(ivalues), std::end(ivalues));
    // 冲击响应频谱--显示波形
    ui->wf2->setTimeGraphData(1, keys, ivalues);
    ui->wf2->setTimeXRange(xlower, xupper);
    ui->wf2->setTimeYRange(ylower, yupper);


    // 滤波数据频谱
    QVector<qreal> svalues;
    frequencySpectrum(m_fValues, svalues);
    keys.clear();
    flen = (svalues.size() - 1) * 2;
    for(int i = 0; i < svalues.size(); i++)
        keys.append(m_freq / flen * i);

    xlower = keys.front();
    xupper = keys.back();
    ylower = *std::min_element(std::begin(svalues), std::end(svalues));
    yupper = *std::max_element(std::begin(svalues), std::end(svalues));
    // 冲击响应频谱--显示波形
    ui->wf3->setTimeGraphData(1, keys, svalues);
    ui->wf3->setTimeXRange(xlower, xupper);
    ui->wf3->setTimeYRange(ylower, yupper);
}

void Widget::on_pushButton_5_clicked()
{
    if(m_fValues.isEmpty())
    {
        QMessageBox::information(this, "提示", "无数据。");
        return;
    }

    QString fileName = QFileDialog::getSaveFileName(this, "保存文件", m_fileName);
    QFile f(fileName);
    if(!f.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox::information(this, "提示", "打开文件失败，无法保存文件。");
        return;
    }
    QString data;
    for(auto d : m_fValues)
    {
        data += QString::number(d);
        data.append(',');
    }
    data.remove(data.length() - 1, 1);
    f.write(data.toUtf8());
    f.close();
}

// 文件类型
void Widget::on_comboBox_currentIndexChanged(int index)
{
    QSettings s("config.ini", QSettings::IniFormat);
    s.setValue("/General/FileType", index);
}

// 滤波器类型
void Widget::on_comboBox_2_currentIndexChanged(int index)
{
    m_filterType = FilterType(index);
    m_filter.setFilterType(m_filterType);

    QSettings s("config.ini", QSettings::IniFormat);
    s.setValue("/Filter/FilterType", m_filterType);
}

// 窗口类型
void Widget::on_comboBox_3_currentIndexChanged(int index)
{
    m_winType = FilterWindowType(index);
    m_filter.setFilterWinType(m_winType);

    QSettings s("config.ini", QSettings::IniFormat);
    s.setValue("/Filter/WinType", m_winType);
}

// 窗口长度
void Widget::on_spinBox_valueChanged(int arg1)
{
    m_winLength = arg1;
    m_filter.setWinLength(m_winLength);

    QSettings s("config.ini", QSettings::IniFormat);
    s.setValue("/Filter/WinLength", m_winLength);
}

// 波形数据频率
void Widget::on_spinBox_2_valueChanged(int arg1)
{
    m_freq = arg1;
    m_filter.setFreqency(m_freq);

    QSettings s("config.ini", QSettings::IniFormat);
    s.setValue("/Filter/Freq", m_freq);
}

// 上限频率
void Widget::on_spinBox_3_valueChanged(int arg1)
{
    m_freqUpLimit = arg1;
    m_filter.setFreqUpper(m_freqUpLimit);

    QSettings s("config.ini", QSettings::IniFormat);
    s.setValue("/Filter/UpFreq", m_freqUpLimit);
}

// 下限频率
void Widget::on_spinBox_4_valueChanged(int arg1)
{
    m_freqLowLimit = arg1;
    m_filter.setFreqLower(m_freqLowLimit);

    QSettings s("config.ini", QSettings::IniFormat);
    s.setValue("/Filter/LowFreq", m_freqLowLimit);
}
