﻿#include "widget.h"
#include "ui_widget.h"
#include <QGraphicsDropShadowEffect>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    // 添加阴影
    QGraphicsDropShadowEffect *e = new QGraphicsDropShadowEffect;
    e->setBlurRadius(10);
    e->setColor(Qt::black);
    e->setOffset(0, 0);
    ui->textEdit->setGraphicsEffect(e);
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_clicked()
{
    // 用一个空指针置换原来的阴影类，达到去掉阴影的效果
    QGraphicsDropShadowEffect *e = Q_NULLPTR;
    ui->textEdit->setGraphicsEffect(e);
}
