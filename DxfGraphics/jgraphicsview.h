﻿#ifndef JGRAPHICSVIEW_H
#define JGRAPHICSVIEW_H

#include <QFrame>
#include <QGraphicsView>
#include <QWheelEvent>
#include <QtMath>


class JGraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit JGraphicsView(QWidget *parent = nullptr);

protected:
    void wheelEvent(QWheelEvent *) override;
public slots:
    void zoomIn(int level = 1);
    void zoomOut(int level = 1);

private slots:
    void setupMatrix();

private:
    int zoomScale;
};

#endif // JGRAPHICSVIEW_H
