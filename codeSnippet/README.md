# Qt 代码片段说明


| 代码片段             | 代码功能说明                                |
| -------------------- | ------------------------------------------- |
| 001-zoomPartWave.cpp | 使用 QCustomPlot 画波形，并实现布局放大功能 |
|                      |                                             |
|                      |                                             |



### 001-zoomPartWave.cpp

1. 使用 QCustomPlot 画波形
2. 并实现布局放大、缩小功能
3. 实现显示、隐藏通道波形通道

![zoomPartWave](https://gitee.com/ALONE_WORK/codes/x5s2heocpydbm0n7lk1zw98/raw?blob_name=1.gif)